# jenkins-docker

This is a test pipeline to create a very basic Alpine image and then push that to my own repo (change to your name for your repo) and then applies that image to a local Kubernetes cluster all using Jenkins locally. Connection requires Kubernetes Plugin that is setup to use a cluster that has been created using the Jenkins user:
```
sudo su jenkins
kind create cluster
```
You don't have to use Kind obviously. Other local cluster options like Minikube will work.
